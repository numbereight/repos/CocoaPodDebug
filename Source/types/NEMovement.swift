//
//  NEMovement.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEMovementState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEMovementState in
            return NEMovement_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEMovement_reprFromState(self))
    }
}
#else
extension NEMovementState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEMovementState in
            return NEMovement_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEMovement_reprFromState(self))
    }
}
#endif

extension NEMovement: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.state = NEMovementState(try decCont.decode(String.self, forKey: .state)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.state.description, forKey: .state)
    }

    enum CodingKeys: String, CodingKey {
        case state
    }
}

#if compiler(>=6.0)
extension NEMovement: @retroactive Equatable {
    public static func == (inLhs: NEMovement, inRhs: NEMovement) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEMovement_isEqual(&lhs, &rhs)
    }
}
#else
extension NEMovement: Equatable {
    public static func == (inLhs: NEMovement, inRhs: NEMovement) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEMovement_isEqual(&lhs, &rhs)
    }
}
#endif
