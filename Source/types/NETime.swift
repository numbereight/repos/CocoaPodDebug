//
//  NETime.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NETimeTime: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NETimeTime in
            return NETime_timeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NETime_reprFromTime(self))
    }
}
#else
extension NETimeTime: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NETimeTime in
            return NETime_timeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NETime_reprFromTime(self))
    }
}
#endif

#if compiler(>=6.0)
extension NETimeType: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NETimeType in
            return NETime_typeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NETime_reprFromType(self))
    }
}
#else
extension NETimeType: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NETimeType in
            return NETime_typeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NETime_reprFromType(self))
    }
}
#endif

extension NETime: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.time = NETimeTime(try decCont.decode(String.self, forKey: .time)) ?? .unknown
        self.type = NETimeType(try decCont.decode(String.self, forKey: .type)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.time.description, forKey: .time)
        try encCont.encode(self.type.description, forKey: .type)
    }

    enum CodingKeys: String, CodingKey {
        case time
        case type
    }
}

#if compiler(>=6.0)
extension NETime: @retroactive Equatable {
    public static func == (inLhs: NETime, inRhs: NETime) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NETime_isEqual(&lhs, &rhs)
    }
}
#else
extension NETime: Equatable {
    public static func == (inLhs: NETime, inRhs: NETime) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NETime_isEqual(&lhs, &rhs)
    }
}
#endif
