//
//  NEConnection.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-03-12.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEConnectionState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEConnectionState in
            return NEConnection_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEConnection_reprFromState(self))
    }
}
#else
extension NEConnectionState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEConnectionState in
            return NEConnection_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEConnection_reprFromState(self))
    }
}
#endif

extension NEConnection: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.state = NEConnectionState(try decCont.decode(String.self, forKey: .state)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.state.description, forKey: .state)
    }

    enum CodingKeys: String, CodingKey {
        case state
    }
}

#if compiler(>=6.0)
extension NEConnection: @retroactive Equatable {
    public static func == (inLhs: NEConnection, inRhs: NEConnection) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEConnection_isEqual(&lhs, &rhs)
    }
}
#else
extension NEConnection: Equatable {
    public static func == (inLhs: NEConnection, inRhs: NEConnection) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEConnection_isEqual(&lhs, &rhs)
    }
}
#endif
