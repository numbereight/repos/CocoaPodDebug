//
//  NELockStatus.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NELockStatusState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NELockStatusState in
            return NELockStatus_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NELockStatus_reprFromState(self))
    }
}
#else
extension NELockStatusState:  LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NELockStatusState in
            return NELockStatus_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NELockStatus_reprFromState(self))
    }
}
#endif

extension NELockStatus: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.state = NELockStatusState(try decCont.decode(String.self, forKey: .state)) ?? .unlocked
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.state.description, forKey: .state)
    }

    enum CodingKeys: String, CodingKey {
        case state
    }
}

#if compiler(>=6.0)
extension NELockStatus: @retroactive Equatable {
    public static func == (inLhs: NELockStatus, inRhs: NELockStatus) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NELockStatus_isEqual(&lhs, &rhs)
    }
}
#else
extension NELockStatus: Equatable {
    public static func == (inLhs: NELockStatus, inRhs: NELockStatus) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NELockStatus_isEqual(&lhs, &rhs)
    }
}
#endif
