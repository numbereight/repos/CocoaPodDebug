//
//  NEIndoorOutdoor.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEIndoorOutdoorState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEIndoorOutdoorState in
            return NEIndoorOutdoor_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEIndoorOutdoor_reprFromState(self))
    }
}
#else
extension NEIndoorOutdoorState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEIndoorOutdoorState in
            return NEIndoorOutdoor_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEIndoorOutdoor_reprFromState(self))
    }
}
#endif

extension NEIndoorOutdoor: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.state = NEIndoorOutdoorState(try decCont.decode(String.self, forKey: .state)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.state.description, forKey: .state)
    }

    enum CodingKeys: String, CodingKey {
        case state
    }
}

#if compiler(>=6.0)
extension NEIndoorOutdoor: @retroactive Equatable {
    public static func == (inLhs: NEIndoorOutdoor, inRhs: NEIndoorOutdoor) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEIndoorOutdoor_isEqual(&lhs, &rhs)
    }
}
#else
extension NEIndoorOutdoor: Equatable {
    public static func == (inLhs: NEIndoorOutdoor, inRhs: NEIndoorOutdoor) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEIndoorOutdoor_isEqual(&lhs, &rhs)
    }
}
#endif
