//
//  NEActivity.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEActivityState: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEActivityState in
            return NEActivity_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEActivity_reprFromState(self))
    }
}
#else
extension NEActivityState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEActivityState in
            return NEActivity_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEActivity_reprFromState(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEActivityMode: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEActivityMode in
            return NEActivity_modeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEActivity_reprFromMode(self))
    }
}
#else
extension NEActivityMode: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEActivityMode in
            return NEActivity_modeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEActivity_reprFromMode(self))
    }
}
#endif

extension NEActivity: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.state = NEActivityState(try decCont.decode(String.self, forKey: .state)) ?? .unknown
        self.modeOfTransport = NEActivityMode(try decCont.decode(String.self, forKey: .modeOfTransport)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.state.description, forKey: .state)
        try encCont.encode(self.modeOfTransport.description, forKey: .modeOfTransport)
    }

    enum CodingKeys: String, CodingKey {
        case state
        case modeOfTransport
    }
}

#if compiler(>=6.0)
extension NEActivity: @retroactive Equatable {
    public static func == (inLhs: NEActivity, inRhs: NEActivity) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEActivity_isEqual(&lhs, &rhs)
    }
}
#else
extension NEActivity: Equatable {
    public static func == (inLhs: NEActivity, inRhs: NEActivity) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEActivity_isEqual(&lhs, &rhs)
    }
}
#endif
